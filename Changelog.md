# Changelog
All notable changes to this project will be documented in this file.



## [v1.1.0] - 30-10-19
### Added

- Changelog.
- Tags

### Changed
- Cabeceras de los archivos.


### Removed
- Directorio Practica 4. 

## [v1.2.0] - 30-10-19.
### Added

- Nueva etiqueta "Practica1"
